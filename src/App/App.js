import React, {Component} from 'react'
import {createStore} from 'redux'
import {Provider} from 'react-redux'

import reducer from '../reducers'
import Routes from './Routes.js'

import '../public/scss/App.css';

const store = createStore(reducer);

class App extends Component {
    render() {
        return (
            <div className="App">
                <Provider store={store}>
                    <Routes />
                </Provider>
            </div>
    );
    }
    }

    export default App;
