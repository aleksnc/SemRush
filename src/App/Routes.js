import React from 'react';
import {Switch, Route} from 'react-router-dom'

import MyPosts from '../pages/MyPosts/MyPosts';
import NewPost from '../pages/NewPost/NewPost';

const Routes = () => (
    <Switch>
        <Route exact path='/' component={MyPosts}/>
        <Route path='/new-post/:number?' component={NewPost}/>
    </Switch>
)

export default Routes;
