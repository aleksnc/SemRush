export default [
    {
        id: '0',
        text: 'draft',
        class: '--draft',
        selected: true,
    },

    {
        id: '1',
        text: 'published',
        class: '--publish',
        selected: false,
    },

    {
        id: '2',
        text: 'approved',
        class: '--approved',
        selected: false,
    },

    {
        id: '3',
        text: 'rejected',
        class: '--reject',
        selected: false,
    },

    {
        id: '4',
        text: 'needs editing',
        class: '--editing',
        selected: false,
    },

    {
        id: '5',
        text: 'waiting for review',
        class: '--forReview',
        selected: false,
    },

    {
        id: '6',
        text: 'under review',
        class: null,
        selected: false,
    },

];
