import React, {Component} from 'react';
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {ADD__CREATE, STATE__POPUP} from '../../constants'
import TransformDate from '../../components/DateWord/TransformDate.js'
import PopUp from '../../components/PopUp/PopUp.js'
import EventLibrary from '../../components/EventLibrary/EventLibrary.js'

class NewPost__btnBlock extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
            numPath: '',
            removeRedirect: false
        }

        this.onSave = this.onSave.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    onSave() {
        let arrayPosts = [];
        let storageArr = JSON.parse(localStorage.getItem("post"));
        let myLocation = window.location.pathname.split('/')[2];
        let state = this.props.getState;
        let num = (myLocation !== "" && myLocation !== undefined) ? myLocation : null;
        let arraySave = (storageArr !== null) ? storageArr : arrayPosts;

        (num !== null) ? arraySave[num] = state : arraySave.push(state);

        localStorage.setItem("post", JSON.stringify(arraySave));

        let arrLeng = (storageArr !== null) ? storageArr.length - 1 : 0;
        let numPath = (num === null) ? arrLeng : 0;

        this.setState({
            numPath: numPath,
            redirect: myLocation === "" || myLocation == undefined
        })

        this.popUP(num, false);
    }

    popUP(num, event) {
        let text = EventLibrary(num, event);

        let content = {
            state: true,
            text: text
        }
        this.props.setData(STATE__POPUP, content);
    }

    onDelete() {
        let storageArr = JSON.parse(localStorage.getItem("post"));
        let newArr = [];
        storageArr.map((item, index) => {
                if (index === parseInt(this.state.numPath)) {
                    return;
                }
                newArr.push(item);
            }
        )

        localStorage.setItem("post", JSON.stringify(newArr));

        this.popUP(this.state.numPath, true);

        this.setState({
            removeRedirect: true
        })
    }


    componentWillMount() {
        console.log("create");
        let createDay = TransformDate(Date.now());
        this.props.setData(ADD__CREATE, createDay);

        this.setState({
            numPath: this.props.num,
            removeRedirect: false
        })
    }

    render() {
        return (
            <div className="NewPost__btnBlock">
                {this.props.getPopUp.state &&
                <PopUp/>
                }
                <div className="NewPost__btnDelete">
                    {this.state.numPath !== null &&
                    <button className="btn Btn--delete" onClick={this.onDelete}> Delete post</button>
                    }
                    {this.state.removeRedirect &&
                    <div>
                        <Redirect from="/new-post" to={"/new-post/"}/>
                    </div>
                    }
                </div>
                <div className="NewPost__btnSave">
                    <button className="Btn Btn--save" onClick={this.onSave}>Save</button>
                    {this.state.redirect &&
                    <div>
                        <Redirect from="/new-post" to={"/new-post/" + this.state.numPath}/>
                    </div>
                    }
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
        getState: state,
        getPopUp: state.popUp,
    }),
    dispatch => ({
        setData: (type, text) => {
            dispatch({type: type, content: text})
        },
    })
)(NewPost__btnBlock);
