import React, {Component} from 'react';
import {Link} from 'react-router-dom'

import UserVisit from '../../components/UserVisit/UserVisit.js'
import Select from '../../components/Select/Select.js'
import UploadImg from '../../components/UploadImg/UploadImg.js'
import TextEditor from '../../components/TextEditor/_TextEditor.js'
import TextBlock from '../../components/TextBlock/TextBlock.js'
import SeoInfo from '../../components/SeoInfo/SeoInfo.js'

import SelectStatus from './SelectStatus.js'
import BtnBlock from './NewPost__btnBlock.js'

class NewPost extends Component {

    constructor(props) {
        super(props);

        this.state = {
            postImage: '',
            mainText: '',
            metaDesc: '',
            metaKey: '',
            metaTitle: '',
            seoImage: '',
            status: [],
            teaser: '',
            changeTab: true,
            num: 'null',
            addTitle: false,
            addLogo: false,
            publicDay: 'sdfd',
        };
    }

    componentWillMount() {
        let num = this.props.match.params.number || null;

        this.setState({
            num: num,
        })

        if (num !== null) {
            let storageArr = JSON.parse(localStorage.getItem("post"));
            let lengthArr = 0;

            if (storageArr !== null) {
                lengthArr = Object.keys(storageArr).length;
            }

            if ((storageArr === null || lengthArr <= num)) {
               window.location = "/new-post"
            }

            if (num >= 0) {
                let pageContent = storageArr[num];
                this.setState({
                    teaser: pageContent.teaser,
                    metaTitle: pageContent.metaTitle,
                    metaDesc: pageContent.metaDesc,
                    metaKey: pageContent.metaKey,
                    mainText: pageContent.mainText,
                    status: pageContent.status,
                    postImage: pageContent.postImage,
                    seoImage: pageContent.seoImage,
                    changeTab: pageContent.changeTab,
                    addTitle: pageContent.addTitle,
                    addLogo: pageContent.addLogo,
                    publicDay: pageContent.publicDay,
                })

                console.log(pageContent);
            }
        }
    }

    render() {

        return (
            <div className="NewPost">

                <div className="NewPost__header">
                    <div className="NewPost__arrBack">
                        <Link className="NewPost__arr" to='/'></Link>
                    </div>
                    <h1 className="NewPost__title">The secrets of Amazon SEO</h1>
                </div>
                <div className="NewPost__user">
                    <UserVisit/>
                </div>
                <div className="NewPost__status">
                    <Select
                        required={true}
                        content={SelectStatus}
                        status={this.state.status}
                        publicDay={this.state.publicDay}
                    />
                </div>
                <div className="NewPost__addImage">
                    <UploadImg img={this.state.postImage}/>
                </div>

                <div className="NewPost__editText">
                    <TextEditor mainText={this.state.mainText}/>

                    <hr/>
                </div>
                <div className="NewPost__meta">
                    <TextBlock
                        datatype="teaser"
                        title="Teaser"
                        linkName="Preview"
                        height={100}
                        placeholder="Teaser"
                        val={this.state.teaser}
                    />
                </div>
                <div className="NewPost__seoInfo">
                    <SeoInfo
                        img={this.state.seoImage}
                        changeTab={this.state.changeTab}
                        addTitle={this.state.addTitle}
                        addLogo={this.state.addLogo}

                    />
                </div>

                <div className="NewPost__meta">
                    <TextBlock
                        datatype="mTitle"
                        title="Meta title"
                        type="input"
                        placeholder="Meta title"
                        val={this.state.metaTitle}
                    />
                </div>

                <div className="NewPost__meta">
                    <TextBlock
                        datatype="mDescription"
                        title="Meta description"
                        height={50}
                        placeholder="With SEMrush Organic Research data, you will
                          find the best SEO keywords and get a higher ranking."
                        val={this.state.metaDesc}
                    />
                </div>

                <div className="NewPost__meta NewPost__meta--last">
                    <TextBlock
                        datatype="mKeywords"
                        title="Meta keywords"
                        height={50}
                        placeholder="SEO, SEMrush, organic research"
                        val={this.state.metaKey}
                    />
                </div>
                <hr/>
                <BtnBlock num={this.state.num}/>
            </div>
        );
    }
}

export default NewPost;
