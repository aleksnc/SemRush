import React, {Component} from 'react';
import {connect} from 'react-redux'
import {ADD__DATABASE, SEARCH__NEWS} from "../../constants";

import {Link} from 'react-router-dom'

import Post from '../../components/Post/Post.js';


class MyPosts extends Component {
    constructor(props) {
        super(props);

        this.onKeyUp = this.onKeyUp.bind(this);
    }

    onKeyUp(e) {
        let text = e.target.value;
        this.props.setSearch(text);
    }

    componentWillMount() {
        let storageArr = JSON.parse(localStorage.getItem("post"));

        if (storageArr == null) {
            return;
        }
        this.props.setDB(storageArr);
    }

    render() {
        console.log(this.props.getStore.mainReducer);

        return (
            <div className="MyPosts">
                <div className="MyPosts__header">
                    <h1 className="MyPosts__title">My posts</h1>
                    <div className="MyPosts__button">
                        <Link className="Btn" to='new-post'>New post</Link>
                    </div>

                    <div className="MyPosts__search">
                        <input
                            type="search"
                            placeholder="Search by title, status, date"
                            onKeyUp={this.onKeyUp}
                        />
                    </div>
                </div>
                <hr/>

                <div className="MyPosts__posts">
                    {
                        Object.keys(this.props.getDB).length !== 0 &&
                        this.props.getDB.map((item, index) => (
                            <div key={index + Date.now().toString()} className="MyPosts__post">
                                <Post content={item} index={index}/>
                            </div>
                        ))
                    }
                    {
                        Object.keys(this.props.getDB).length === 0 &&
                        <h2 className="MyPosts__title">Ooops... There is nothing</h2>
                    }
                </div>
            </div>
        );
    }
}


export default connect(
    state => ({
        getDB: state.mainReducer.filter(getSearch => (
                getSearch.teaser.includes(state.search) ||
                getSearch.status.text.includes(state.search) ||
                getSearch.createDay.includes(state.search) ||
                getSearch.publicDay.includes(state.search)
            )
        ),
        getSearch: state.search,
        getStore: state
    }),
    dispatch => ({
        setDB: (content) => {
            dispatch({type: ADD__DATABASE, content: content})
        },
        setSearch: (content) => {
            dispatch({type: SEARCH__NEWS, content: content})
        },

    })
)(MyPosts);
