export const ADD__IMAGE = 'ADD__IMAGE';
export const ADD__IMAGEm = 'ADD__IMAGEm';
export const ADD__MAINTEXT = 'ADD__MAINTEXT';
export const ADD__TEASER = 'ADD__TEASER';
export const ADD__STATUS = 'ADD__STATUS';
export const ADD__BLOCK = 'ADD__BLOCK';
export const ADD__TITLE = 'ADD__TITLE';
export const ADD__LOGO = 'ADD__LOGO';
export const CHANGE__TAB = 'CHANGE__TAB';
export const ADD__PUBLIC = 'ADD__PUBLIC';
export const ADD__CREATE = 'ADD__CREATE';
export const STATE__POPUP = 'STATE__POPUP';

export const ADD__METATITLE = 'ADD__METATITLE';
export const ADD__METADESC = 'ADD__METADESC';
export const ADD__METAKEY = 'ADD__METAKEY';

export const SEARCH__NEWS = 'SEARCH__NEWS';
export const ADD__DATABASE = 'ADD__DATABASE';
export const REMOVE__BLOCK = 'REMOVE__BLOCK';

