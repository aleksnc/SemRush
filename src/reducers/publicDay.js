import {ADD__PUBLIC} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__PUBLIC) {
        return action.content
    }
    return state;
}

export default reducers