import {ADD__METAKEY} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__METAKEY) {
        return action.content
    }
    return state;
}

export default reducers