import {SEARCH__NEWS} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === SEARCH__NEWS) {
        return action.content
    }
    return state;
}

export default reducers
