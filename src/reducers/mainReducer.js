import {ADD__DATABASE} from "../constants";

const initialState = [];

function reducers(state = initialState, action) {
    if (action.type === ADD__DATABASE) {
        return action.content
    }
    return state;
}

export default reducers