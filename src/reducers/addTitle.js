import {ADD__TITLE} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__TITLE) {
        return action.content
    }
    return state;
}

export default reducers