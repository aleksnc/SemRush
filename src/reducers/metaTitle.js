import {ADD__METATITLE} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__METATITLE) {
        return action.content
    }
    return state;
}

export default reducers