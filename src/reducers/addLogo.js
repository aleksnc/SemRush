import {ADD__LOGO} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__LOGO) {
        return action.content
    }
    return state;
}

export default reducers