import {ADD__METADESC} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__METADESC) {
        return action.content
    }
    return state;
}

export default reducers