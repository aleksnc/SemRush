import {ADD__IMAGE} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__IMAGE) {
        return action.content
    }
    return state;
}

export default reducers
