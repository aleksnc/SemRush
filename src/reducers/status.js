import {ADD__STATUS} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__STATUS) {
        return action.content
    }
    return state;
}

export default reducers
