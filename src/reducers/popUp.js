import {STATE__POPUP} from "../constants";

const initialState = {
    state: false,
    text:''
};

function reducers(state = initialState, action) {
    if (action.type === STATE__POPUP) {
        return action.content
    }
    return state;
}

export default reducers