import {CHANGE__TAB} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === CHANGE__TAB) {
        return action.content
    }
    return state;
}

export default reducers
