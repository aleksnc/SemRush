import {ADD__MAINTEXT} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__MAINTEXT) {
        return action.content
    }
    return state;
}

export default reducers
