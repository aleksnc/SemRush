import {ADD__IMAGEm} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__IMAGEm) {
        return action.content
    }
    return state;
}

export default reducers
