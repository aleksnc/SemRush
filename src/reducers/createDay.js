import {ADD__CREATE} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__CREATE) {
        console.log(action.content);
        return action.content
    }
    return state;
}

export default reducers