import {ADD__TEASER} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__TEASER) {
        return action.content
    }
    return state;
}

export default reducers