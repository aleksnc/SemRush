import {combineReducers} from 'redux'
import postImage from './postImage.js';
import seoImage from './seoImage.js';
import mainText from './mainText.js';
import metaKey from './metaKey.js';
import metaDesc from './metaDesc.js';
import teaser from './teaser.js';
import metaTitle from './metaTitle.js';
import status from './status.js';
import mainReducer from './mainReducer.js';
import search from './search.js';
import changeTab from './changeTab.js';
import addLogo from './addLogo.js';
import addTitle from './addTitle.js';
import publicDay from './publicDay.js';
import createDay from './createDay.js';
import popUp from './popUp.js';



export default combineReducers({
    postImage,
    seoImage,
    mainText,
    metaKey,
    metaDesc,
    teaser,
    metaTitle,
    status,
    mainReducer,
    search,
    changeTab,
    addTitle,
    publicDay,
    createDay,
    addLogo,
    popUp
})