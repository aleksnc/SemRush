import {ADD__BLOCK, REMOVE__BLOCK} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === ADD__BLOCK) {
        return action.content
    }
    return state;
}

export default reducers