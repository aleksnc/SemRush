import React from 'react';

const UserVisit = () => (
    <div className="UserVisit">
        <img className="UserVisit__image" src={require("../../public/images/foto.jpg")} alt="user"/>
        <span className="UserVisit__name">Vault Boy</span>
    </div>
)

export default UserVisit;