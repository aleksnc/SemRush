import React from 'react';

const PreviewText = (props) => {

    const {title=true, info=true, desc=true} = props;
    return (
        <div className="UploadImg__previewText">

            <img src={require("../../public/images/icon/upload.png")} alt=""/>
            {
                title &&
                <h2 className="UploadImg__title">Featured image </h2>
            }
            {
                info &&
                <span className="UploadImg__info">select an image file to upload or drag it here</span>
            }
            {
                desc &&
                <p className="UploadImg__description">Please see the requirements for our Featured image. If you are
                    unsure
                    what
                    image you should select, <br/>
                    do not upload anything. Our editor will upload an appropriate image when reviewing your post.</p>
            }
        </div>
    )
}

export default PreviewText;
