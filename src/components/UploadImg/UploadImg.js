import React, {Component} from 'react';
import {connect} from 'react-redux'
import {ADD__IMAGE, ADD__IMAGEm} from '../../constants'


import PreviewText from './UploadImg__previewText'

class UploadImg extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreviewUrl: '',
        };


        this.onRemove = this.onRemove.bind(this);
    }

    onRemove(){
        let types = (this.props.mod !== '') ? ADD__IMAGEm : ADD__IMAGE;
        this.props.setImage(types, '');
    }

    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];
        if (file === undefined) {
            return false;
        }
        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });

            let types = (this.props.mod !== '') ? ADD__IMAGEm : ADD__IMAGE;
            this.props.setImage(types, reader.result);
        }

        reader.readAsDataURL(file)
    }

    componentWillMount() {
        this.setState({
            imagePreviewUrl: this.props.img
        });


        let types = (this.props.mod !== '') ? ADD__IMAGEm : ADD__IMAGE;
        this.props.setImage(types, this.props.img);
    }

    render() {
        let imagePreviewUrl = (this.props.mod !== '') ? this.props.getImageSEO : this.props.getImage;
        let imagePreview = null;
        let imageRemove = false;
        if (imagePreviewUrl) {
            imagePreview = (<img src={imagePreviewUrl} alt=" "/>);
            imageRemove = true;
        } else {
            imagePreview = <PreviewText title={this.props.title} desc={this.props.desc}/>;
        }

        return (
            <div className={"UploadImg " + this.props.mod}>
                {imageRemove &&
                <div className="UploadImg__remove" onClick={this.onRemove}>
                    remove
                </div>
                }
                <div className="UploadImg__wrapper">
                    <input className="UploadImg__input"
                           type="file"
                           onChange={(e) => this._handleImageChange(e)}/>

                    <div className="UploadImg__preview">
                        {imagePreview}
                    </div>
                    <div className="UploadImg__attention"><span>This area will not be displayed on the post page</span>
                    </div>
                </div>
            </div>
        )
    }
}

UploadImg.defaultProps = {
    mod: ''
};
export default connect(
    state => ({
        getImage: state.postImage,
        getImageSEO: state.seoImage,
    }),
    dispatch => ({
        setImage: (type, img) => {
            dispatch({type: type, content: img})
        },
    })
)(UploadImg);