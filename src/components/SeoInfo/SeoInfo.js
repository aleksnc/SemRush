import React, {Component} from 'react';

import SocialMedia from '../SocialMedia/SocialMedia.js';

class SeoInfo extends Component {
    render() {
        return (
            <div className="SeoInfo">
                <div className="SeoInfo__title">SEO information</div>
                <div className="SeoInfo__description">Here you can specify information that will help search engines
                    discover your post and display it in search results. It is also used when sharing
                    a post on social media. If you're unsure, leave the fields empty.
                </div>
                <div className="SeoInfo__media">
                    <SocialMedia
                        img={this.props.img}
                        changeTab={this.props.changeTab}
                        addTitle={this.props.addTitle}
                        addLogo={this.props.addLogo}
                    />
                </div>

            </div>
        );
    }
}

export default SeoInfo;
