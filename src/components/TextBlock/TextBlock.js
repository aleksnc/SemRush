import React, {Component} from 'react';
import {connect} from 'react-redux'
import {ADD__METAKEY, ADD__TEASER, ADD__METATITLE, ADD__METADESC} from '../../constants'

class TextBlock extends Component {
    constructor(props) {
        super(props);


        this.onKeyUp = this.onKeyUp.bind(this);
    }


    onKeyUp(e) {
        let text = e.target.value;

        let dataType = this.props.datatype
        let type = '';

        switch (dataType) {
            case 'teaser':
                type = ADD__TEASER;
                break;
            case 'mTitle':
                type = ADD__METATITLE;
                break;
            case 'mDescription':
                type = ADD__METADESC;
                break;
            case 'mKeywords':
                type = ADD__METAKEY;
                break;
            default:
                break;
        }

        this.props.setText(type, text);
    }

    componentWillMount() {

        let dataType = this.props.datatype;
        let type = '';

        switch (dataType) {
            case 'teaser':
                type = ADD__TEASER;
                break;
            case 'mTitle':
                type = ADD__METATITLE;
                break;
            case 'mDescription':
                type = ADD__METADESC;
                break;
            case 'mKeywords':
                type = ADD__METAKEY;
                break;
            default:
                break;
        }


        this.props.setText(type, this.props.val);
    }

    render() {
        return (
            <div className="TextBlock">
                {
                    this.props.title !== null &&
                    <div className="TextBlock__title">{this.props.title}</div>
                }
                {
                    this.props.linkName !== null &&
                    <div className="TextBlock__headLink">
                        <a href="#">{this.props.linkName}</a>
                    </div>
                }
                <div className="TextBlock__text">
                    {
                        this.props.type === 'textarea' &&
                        <textarea
                            data-type={this.props.datatype}
                            className="TextBlock__content"
                            style={{"minHeight": this.props.height + "px"}}
                            placeholder={this.props.placeholder}
                            defaultValue={this.props.val}
                            onKeyUp={this.onKeyUp}
                        />
                    }
                    {
                        this.props.type === 'input' &&
                        <input
                            data-type={this.props.datatype}
                            type="text"
                            className="TextBlock__content"
                            placeholder={this.props.placeholder}
                            defaultValue={this.props.val}
                            onKeyUp={this.onKeyUp}
                        />
                    }
                </div>
            </div>
        );
    }
}

TextBlock.defaultProps = {
    title: null,
    link: null,
    height: 20,
    type: 'textarea',
    placeholder: '',
    datatype: '',
    val: ''
};

export default connect(
    state => ({
        getText: state.teaser
    }),
    dispatch => ({
        setText: (type, text) => {
            dispatch({type: type, content: text})
        },
    })
)(TextBlock);
