import React, {Component} from 'react';
import {connect} from 'react-redux'
import {CHANGE__TAB, ADD__TITLE, ADD__LOGO} from '../../constants'


import UploadImg from '../../components/UploadImg/UploadImg.js'
import MediaImg from '../../components/MediaImg/MediaImg.js'

class SocialMedia extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: '',
            logo: '',
            image: '',
            upload: '',
            TabLeft: true
        };
        this.onChangeTab = this.onChangeTab.bind(this);
        this.onChangeCheck = this.onChangeCheck.bind(this);
    }

    onChangeTab(e) {
        let val = e.target.value;
        let param = val === 'true';
        this.setState({
            TabLeft: param
        });

        this.props.setData(CHANGE__TAB, param);
    }

    onChangeCheck(e) {
        let isCheck = e.target.checked;
        let event = e.target.id;

        let val = (isCheck) ? e.target.value : false;
        let type = '';

        switch (event) {
            case 'isTitle':
                this.setState({
                    title: val
                });
                type = ADD__TITLE;
                break;
            case 'isLogo':
                this.setState({
                    logo: val
                });
                type = ADD__LOGO;
                break;
            default:
                break;
        }

        this.props.setData(type, val);
    }

    componentWillMount() {
        let param = (this.props.changeTab !== '') ? this.props.changeTab : true;
        let title = (this.props.addTitle !== '') ? this.props.addTitle : false;
        let logo = (this.props.addLogo !== '') ? this.props.addLogo : false;
        this.setState({
            TabLeft: param,
            title: title,
            logo: logo
        });

        this.props.setData(CHANGE__TAB, param);
        this.props.setData(ADD__TITLE, title);
        this.props.setData(ADD__LOGO, logo);
    }

    render() {
        return (
            <div className="SocialMedia">
                <div className="SocialMedia__title">Image to be displayed on social media</div>

                <div className="SocialMedia__tabs">
                    <div className="SocialMedia__tab">
                        <input
                            type="radio"
                            defaultChecked={this.state.TabLeft}
                            name="type" id="auto"
                            value={true}
                            onChange={this.onChangeTab}
                        />
                        <label htmlFor="auto">Create automatically</label>
                    </div>
                    <div className="SocialMedia__tab">
                        <input
                            type="radio"
                            defaultChecked={!this.state.TabLeft}
                            name="type"
                            id="manual"
                            value={false}
                            onChange={this.onChangeTab}
                        />
                        <label htmlFor="manual">Upload manually</label>
                    </div>
                </div>
                {
                    this.state.TabLeft &&
                    <div className="SocialMedia__tabContent">
                        <div className="SocialMedia__checkBlock">
                            <input
                                type="checkbox"
                                id="isTitle"
                                value={"SEO information"}
                                checked={this.state.title}
                                onChange={this.onChangeCheck}
                            />
                            <label htmlFor="isTitle">
                                <div className="checkbox">checkbox</div>
                                <span>Add title</span></label>
                        </div>
                        <div className="SocialMedia__checkBlock">
                            <input
                                type="checkbox"
                                id="isLogo"
                                value="MediaImg__content--logo"
                                checked={this.state.logo}
                                onChange={this.onChangeCheck}
                            />
                            <label htmlFor="isLogo">
                                <div className="checkbox">checkbox</div>
                                <span>Add logo</span></label>
                        </div>
                        <div className="SocialMedia__img">
                            <MediaImg
                                title={this.state.title}
                                logo={this.state.logo}
                            />
                        </div>
                    </div>
                }
                {
                    !this.state.TabLeft &&
                    <div className="SocialMedia__tabContent">
                        <div className="SocialMedia__img">
                            <UploadImg title={false} desc={false} mod="UploadImg--min" img={this.props.img}/>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        setData: (type, param) => {
            dispatch({type: type, content: param})
        },
    })
)(SocialMedia);
