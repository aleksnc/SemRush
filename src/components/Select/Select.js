import React, {Component} from 'react';
import {connect} from 'react-redux'
import {ADD__STATUS, ADD__PUBLIC} from '../../constants'

import TransformDate from '../DateWord/TransformDate.js'

class SelectForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: props.content[0].text,
            value: '',
            className: props.content[0].class,
            options: ''
        };

        this.onChangeSelect = this.onChangeSelect.bind(this);
    }

    onChangeSelect(e) {
        let n = e.target.options.selectedIndex;
        let text = e.target.options[n].text;
        let val = e.target.options[n].value;
        let className = this.props.content[n].class;

        this.setState({
            text: text,
            value: val,
            className: className,
        });

        let content = {
            text: text,
            value: val,
            className: className,
        };

        this.props.setStatus(ADD__STATUS,content);

        let state = this.props.getState;
        let date =TransformDate(Date.now());
        let publicDay = (e.target.options[n].text === 'published') ? date : '';
        this.props.setStatus(ADD__PUBLIC, publicDay);
    }

    componentWillMount() {
        let content = {
            text: this.state.text,
            value: this.state.value,
            className: this.state.className,
        };

        if (this.props.status.length !== 0) {
            this.setState({
                text: this.props.status.text,
                value: this.props.status.value,
                className: this.props.status.className,
            })

            content = {
                text: this.props.status.text,
                value: this.props.status.value,
                className: this.props.status.className,
            };
        }

        let options = [];
        this.props.content.map((item, index) => (
            options.push(<option key={item.id + index.class} value={item.id + ""}>{item.text}</option>)
        ));

        this.setState({
            options: options
        });

        this.props.setStatus(ADD__PUBLIC, this.props.publicDay);
        this.props.setStatus(ADD__STATUS,content);
    }

    render() {
        return (
            <div className="Select__selectWrapper">
                <div className="Select__selectBlock">
                        <span
                            className={"Select__select" + (this.state.className !== null ? " Select__select" + this.state.className : "")}>
                            {this.state.text}
                        </span>

                    <select
                        value={this.state.value}
                        className="Select__select"
                        onChange={this.onChangeSelect}
                    >
                        {this.state.options}
                    </select>
                </div>
            </div>
        );
    }
}

SelectForm.defaultProps = {required: 'false'};
export default connect(
    state => ({
        getStatus: state.status
    }),
    dispatch => ({
        setStatus: (type,status) => {
            dispatch({type: type, content: status})
        },
    })
)(SelectForm);
