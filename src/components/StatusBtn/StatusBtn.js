import React, { Component } from 'react';

class StatusBtn extends Component {
    render() {

        const {
            className,
            text,
        } = this.props.status;


        return (
            <div className={"StatusBtn "+(className!==""?"StatusBtn"+className:"")}>
                <span>{text}</span>
                <span className={"StatusBtn__i "+(className!==""?"StatusBtn__i"+className:"")}>i</span>
                <div className="StatusBtn__description">
                    Reason: Your article quality is of low value or
                    the topic has been covered extensively already.
                </div>
            </div>
        );
    }
}

export default StatusBtn;
