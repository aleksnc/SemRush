import React, {Component} from 'react'
import {connect} from 'react-redux'
import {STATE__POPUP} from '../../constants'


class PopUp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            popup: ''
        };
    }

    popUp = (t) => {
        function func() {
            const content = {
                state: false,
                text: ''
            }

           t.props.setContent(STATE__POPUP, content)

        }

        setTimeout(func, 3000);
    };

    componentWillMount() {
        this.popUp(this)
    }

    render() {
        return (
            <div className="PopUp">
                {this.props.getContent.text}
            </div>
        );
    }
}

export default connect(
    state => ({
        getContent: state.popUp
    }),
    dispatch => ({
        setContent: (type, text) => {
            dispatch({type: type, content: text})
        },
    })
)(PopUp);
