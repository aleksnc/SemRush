import React, {Component} from 'react';
import {connect} from 'react-redux'
import {ADD__MAINTEXT} from '../../constants'

import RichTextEditor from 'react-rte';

class TextEditor extends Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);

        this.state = {
            value: RichTextEditor.createValueFromString(props.mainText, 'html')
        };
    }

    onChange = (value) => {
        this.setState({value});
        let text = value.toString('html');
        this.props.setText(text);
    };

    componentWillMount() {
        this.props.setText(this.state.value.toString('html'));
    }

    render() {
        return (
            <div className="TextEditor">
                <RichTextEditor
                    value={this.state.value}
                    onChange={this.onChange}
                />
            </div>
        );
    }
}

export default connect(
    state => ({
        getText: state.mainText
    }),
    dispatch => ({
        setText: (text) => {
            dispatch({type: ADD__MAINTEXT, content: text})
        },
    })
)(TextEditor);
