import React from 'react';
import DateWord from '../DateWord/DateWord.js'

const TransformDate = (date) => {
    if (date === '') {
        return;
    }

    let myDate = new Date(parseInt(date));

    let transform = DateWord[0].Months[myDate.getMonth()] + " " + myDate.getDate() + ", " + myDate.getFullYear() + ", " + myDate.getHours() + ":" + myDate.getMinutes();
    return transform;
}

export default TransformDate


