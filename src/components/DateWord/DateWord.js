export default [
    {
        'Days': [
            "Sunday", "Monday", "Tuesday", "Среда", "Wednesday", "Пятница", "Saturday"
        ],
        'Months': ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"]
    }
]