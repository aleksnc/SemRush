import React, {Component} from 'react';
import {connect} from 'react-redux'

class MediaImg extends Component {
    render() {

        return (
            <div className="MediaImg">
                {
                    this.props.getImage !== false &&
                    <img className="MediaImg__img" src={this.props.getImage} alt={this.props.getTitle}/>
                }
                {
                    (this.props.logo !== false || (this.props.title !== false && this.props.getTitle !== false)) &&
                        <div className={"MediaImg__content " + this.props.logo}>
                            {
                                (this.props.title !== false && this.props.getTitle !== false) &&
                                <p className="MediaImg__title">{this.props.getTitle}</p>
                            }
                        </div>
                }
            </div>
        );
    }
}

MediaImg.defaultProps = {
    logo: '',
    title: ''
};
export default connect(
    state => ({
        getImage: state.postImage,
        getTitle: state.teaser,
    })
)(MediaImg);
