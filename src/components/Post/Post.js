import React, {Component} from 'react';
import StatusBtn from '../StatusBtn/StatusBtn.js'

class Post extends Component {

    constructor(props) {
        super(props);

        this.state = {
            publicType: '',
            renderDay: ''
        }
    }


    componentWillMount() {
        let publickDay = this.props.content.publicDay;
        let createDay = this.props.content.createDay;
        let renderDay = (this.props.content.status.text === "published") ? publickDay : createDay;
        let publicType = this.props.content.status.text !== "published";

        this.setState({
            publicType: publicType,
            renderDay: renderDay
        });
    }


    render() {
        const {
            postImage,
            status,
            teaser,
        } = this.props.content;

        return (
            <div className="Post">
                <a href={"/new-post/" + this.props.index}>
                    <div className="Post__img">
                        {(postImage !== null || postImage !== '') &&
                        <img src={postImage} alt="teaser"/>
                        }
                    </div>
                </a>

                <div className="Post__status">
                    <StatusBtn status={status}/>
                </div>
                <div className="Post__date">
                    {this.state.publicType &&
                    <span>Updated: </span>
                    }
                    <span>{this.state.renderDay}</span>
                </div>
                <div className="Post__seo">
                    <span>Content marketing</span>
                </div>
                <a href={"/new-post/" + this.props.index} className="Post__title">
                    {teaser}
                </a>

                <div className="Post__blockInfo">
                    {status.text === 'published' &&
                    <div>
                        <div className="Post__info Post__info--views">
                            <span>{this.props.views}</span>
                        </div>
                        <div className="Post__info Post__info--stars">
                            <span>{this.props.stars}</span>
                        </div>
                        <div className="Post__info Post__info--repost">
                            <span>{this.props.repost}</span>
                        </div>
                    </div>
                    }
                </div>
            </div>
        );
    }
}

Post.defaultProps = {
    postImage: null,
    views: 0,
    stars: 0,
    repost: 0,
    status: 'draft',
};
export default Post;
